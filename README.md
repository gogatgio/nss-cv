# NSS example

By Giorgi Gogatishvili

A simple gitflow example

## Technologies Used
- Git
- GitLab
- Brain

## Description

NSS gitflow homework with all the steps.

## Setup/Installation Requirements

- Install Git
- Set up GitLab SSH
- Clone this repository
- View whatever you want

## Copyright

@ Giorgi Gogatishvili
